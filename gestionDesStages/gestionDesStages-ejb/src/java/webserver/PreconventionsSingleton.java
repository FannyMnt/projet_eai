/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webserver;

import java.util.ArrayList;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.inject.Inject;
import javax.jms.JMSContext;
import javax.jms.ObjectMessage;
import javax.jms.Topic;
import messages.Enseignant;
import messages.EtatPreConvention;
import messages.Preconvention;
import messages.RepMessage;

/**
 * Utilisation de la préconvention sur le webserver
 * @author Montuy & Martel
 */
@Singleton
@LocalBean
public class PreconventionsSingleton {
    @Resource(lookup = "TOPIC_PREV_EMISES")
    private Topic topic;
    
    @Inject
    private JMSContext context;
    
    @EJB
    EnseignantsSingleton enseignants;
    
    /**
     * Liste des préconventions créées
     */
    private final ArrayList<Preconvention> preconventions = new ArrayList<>();
    
    /**
     * Permet de créer une préconvention 
     * @param pNomAssurance Assurance
     * @param pNumContrat Numéro de contrat
     * @param pDateD date de début
     * @param pDateF date de fin
     * @param pGratification gratification
     * @param pResume résumé du stage
     * @param pNiveauD niveau de diplome de l'élève
     * @param pIntituleD intitulé du diplome
     * @param pNumE numéro de l'étudiant
     * @param pNomE nom de l'étudiant
     * @param pPrenomE prénom de l'étudiant 
     * @param pNumEnt numéro de SIREN de l'entreprise
     * @param pNomEnt nom de l'entreprise
     * @return 
     */
    public Preconvention creerPreconvention(String pNomAssurance, String pNumContrat, String pDateD, String pDateF,float pGratification, String pResume, String pNiveauD, String pIntituleD, String pNumE, String pNomE, String pPrenomE, String pNumEnt, String pNomEnt ) {
        System.out.println("webserver.PreconventionsSingleton.creerPreconvention() " + preconventions.size());
        Preconvention prev = new Preconvention(preconventions.size(), pNomAssurance,  pNumContrat,  pDateD,  pDateF, pGratification,  pResume, pNiveauD, pIntituleD , pNumE, pNomE, pPrenomE, pNumEnt, pNomEnt);
        System.out.println("webserver.PreconventionsSingleton.creerPreconvention() prev créé " + prev.toString());
        preconventions.add(prev);
        ObjectMessage om = context.createObjectMessage(prev);
        context.createProducer().send(topic, prev);
        return prev;
    }
        
    /**
     * Renvoie la prconvention d'id idPrev
     * @param idPrev id de la préconvention
     * @return 
     */
    public Preconvention recupPreconvention(int idPrev) {
        Preconvention prev = preconventions.get(idPrev);
        return prev;
    }
    
    /**
     * Renvoie la liste des préconventions validées
     * @return 
     */
    public ArrayList<Preconvention> recupListePreconventions() {
        ArrayList<Preconvention> listeEnvoie = new ArrayList<>();
        for(Preconvention p : preconventions){
            if(p.getEtat()==EtatPreConvention.VALIDEE)
                listeEnvoie.add(p);
        }
        return listeEnvoie;
    }
    
    /**
     * Permet d'ajouter un enseignant à la préconvention
     * @param idPrev id de la préconvention
     * @param idEnseignant numéro de l'enseignant
     * @return 
     */
    public Preconvention ajouterEnseignantPreconvention(int idPrev, int idEnseignant) {
        Preconvention prev = preconventions.get(idPrev);
        Enseignant e = enseignants.getEnseignant().get(idEnseignant);
        //prev.setEnseignant(e);
        return prev;
    }
    
    /**
     * Permet de mettre à jour une préconvention
     * @param repM
     * @return 
     */
    public Preconvention miseAJourPrev(RepMessage repM) {
        Preconvention prev = preconventions.get(repM.getIdPreconventon());
        prev.setEtat(repM.getEtat());
        prev.setMotif(repM.getMotif());
        prev.setEnseignant(repM.getEnseignant());
        return prev;
    }

    /**
     * Renvoie la liste des entreprise où un stage s'éffectue
     * @return 
     */
    public ArrayList<String> recupListeEntreprises() {
       ArrayList<String> listeEnvoie = new ArrayList<>();
        for(Preconvention p : preconventions){
            if(p.getEtat()==EtatPreConvention.VALIDEE)
                listeEnvoie.add(p.getNomEnt());
        }
        return listeEnvoie;
    }

    /**
     * Renvoie la liste des préconventions qui n'ont pas été validée.
     * @return 
     */
    public ArrayList<Preconvention> recupListeStageNOK() {
        ArrayList<Preconvention> listeEnvoie = new ArrayList<>();
        System.out.println("Test");
        for(Preconvention p : preconventions){
            System.out.println(p.getEtat());
            if(p.getEtat()==EtatPreConvention.ANNULEE)
                listeEnvoie.add(p);
                System.out.println("NOK");
        }
        return listeEnvoie;
    }

    /**
     * Renvoie le nombre de stage validée.
     * @return 
     */
    public int getNombreStageValidée() {
       ArrayList<Preconvention> listeEnvoie = new ArrayList<>();
       int nb = 0;
        for(Preconvention p : preconventions){
            if(p.getEtat()==EtatPreConvention.VALIDEE)
                nb= nb+1;
        }
        return nb;
    }
}
