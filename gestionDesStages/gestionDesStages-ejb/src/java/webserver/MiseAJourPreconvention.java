/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webserver;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.EJB;
import javax.ejb.MessageDriven;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;
import messages.Preconvention;
import messages.RepMessage;

/**
 * Serveur WEB (Listener de la gestion des stages)
 * @author Montuy & Martel
 */
@MessageDriven(activationConfig = {
    @ActivationConfigProperty(propertyName = "destinationLookup", propertyValue = "TOPIC_GEST_PREV")
    ,
        @ActivationConfigProperty(propertyName = "subscriptionName", propertyValue = "TOPIC_GEST_PREV")
    ,
        @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Topic")
})
public class MiseAJourPreconvention implements MessageListener {
    
    @EJB
    PreconventionsSingleton prevSingleton;
    /**
     * Constructeur par défault
     */
    public MiseAJourPreconvention() {
    }
    
    @Override
    public void onMessage(Message message) {
        if (message instanceof ObjectMessage) {
             try {
                 ObjectMessage om = (ObjectMessage) message;
                 Object obj = om.getObject();
                 if (obj instanceof RepMessage) {
                     RepMessage repM = (RepMessage) obj;
                     System.out.println("RepMessage " + repM.getIdPreconventon()+ " traitée reçue");
                     System.out.println("Etat " + repM.getEtat());
                     Preconvention prev = prevSingleton.miseAJourPrev(repM);
                     System.out.println("Préconvention mise à jour !");
                     System.out.println(prev.toString());
                 }
             } catch (JMSException ex) {
                 Logger.getLogger(MiseAJourPreconvention.class.getName()).log(Level.SEVERE, null, ex);
             }
        }
    }
    
}
