/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webserver;

import java.util.ArrayList;
import javax.ejb.Singleton;
import javax.ejb.LocalBean;
import messages.Enseignant;

/**
 * Permet de créer des enseignants
 * @author Montuy & Martel
 */
@Singleton
@LocalBean
public class EnseignantsSingleton {
    
    private final ArrayList<Enseignant> enseignants;

    public EnseignantsSingleton() {
        enseignants = new ArrayList<Enseignant>();
        // creation des enseignants
        Enseignant e1 = new Enseignant("TECE12", "Teyssie", "Cedric");
        Enseignant e2 = new Enseignant("TOPA12", "Torguet", "Patrice");
        Enseignant e3 = new Enseignant("LAEM12", "Lavinal", "Emmanuel");
        Enseignant e4 = new Enseignant("TEOL12", "Test", "Olivier");
        enseignants.add(e1);
        enseignants.add(e2);
        enseignants.add(e3);
        enseignants.add(e4);
    }

    public ArrayList<Enseignant> getEnseignant() {
        return enseignants;
    }

}
