/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package messages;

import java.io.Serializable;

/**
 * Objet RepMessage (Réponse des différences services)
 * @author Montuy & Martel
 */
public class RepMessage implements Serializable {
    
    private int idPreconventon;
    private EtatPreConvention etat;
    private String motif;
    private String enseignant;

    /**
     * Constructeur de la réponse des services Juridique et Scolarité
     * @param idPreconventon id de la préconvention
     * @param etat etat de la préconvention
     * @param motif motif du refus ou ""
     */
    public RepMessage(int idPreconventon, EtatPreConvention etat, String motif) {
        this.idPreconventon = idPreconventon;
        this.etat = etat;
        this.motif = motif;
    }
    
    /**
     * Constructeur de la réponse du service pédagogique et gestion des stages
     * @param idPreconventon id de la préconvention
     * @param etat etat de la préconvention
     * @param motif motif du refus ou ""
     * @param enseignant enseignant-chercheur qui doit suivre le stage
     */
    public RepMessage(int idPreconventon, EtatPreConvention etat, String motif, String enseignant) {
        this.idPreconventon = idPreconventon;
        this.etat = etat;
        this.motif = motif;
        this.enseignant = enseignant;
    }

    public String getEnseignant() {
        return enseignant;
    }

    public void setEnseignant(String enseignant) {
        this.enseignant = enseignant;
    }

    public int getIdPreconventon() {
        return idPreconventon;
    }

    public void setIdPreconventon(int idPreconventon) {
        this.idPreconventon = idPreconventon;
    }

    public EtatPreConvention getEtat() {
        return etat;
    }

    public void setEtat(EtatPreConvention etat) {
        this.etat = etat;
    }

    public String getMotif() {
        return motif;
    }

    public void setMotif(String motif) {
        this.motif = motif;
    }

    @Override
    public String toString() {
        return "Message{" + "idPreconventon=" + idPreconventon + ", etat=" + etat + ", motif=" + motif + '}';
    }
    
    
    
}
