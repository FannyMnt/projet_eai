/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package messages;

import java.io.Serializable;

/**
 * Objet Preconvention
 * @author Montuy & Martel
 */
public class Preconvention implements Serializable {
    
    private int idPreconvention;
    
    private String nomAssurance;
    private String numContrat;
    private String dateD;
    private String dateF;
    private float gratification;
    private String resume;
    
    private EtatPreConvention etat;
    private String motif;

    private String enseignant;
    
    private String niveauD;
    private String intituleD;
    private String numE;
    private String nomE;
    private String prenomE;
    private String numEnt;
    private String nomEnt;
    
    public Preconvention() {}
        
    public Preconvention(int idPreconventon, String nomAssurance) {
        this.idPreconvention = idPreconventon;
        this.nomAssurance = nomAssurance;
    }
    
    public Preconvention(int pIdPreconvention, String pNomAssurance, String pNumContrat, String pDateD, String pDateF,float pGratification, String pResume, String pNiveauD, String pIntituleD, String pNumE, String pNomE, String pPrenomE, String pNumEnt, String pNomEnt ) {
        this.idPreconvention = pIdPreconvention;
        this.nomAssurance = pNomAssurance;
        this.numContrat = pNumContrat;
        this.dateD = pDateD;
        this.dateF = pDateF;
        this.gratification = pGratification;
        this.resume = pResume;
        
        this.niveauD = pNiveauD;
        this.intituleD = pIntituleD;
        this.numE = pNumE ;
        this.nomE = pNomE;
        this.prenomE = pPrenomE;
        this.numEnt = pNumEnt;
        this.nomEnt = pNomEnt;
        this.etat = etat.EN_COURS_DE_TRAITEMENT;
    }

    public int getIdPreconvention() {
        return idPreconvention;
    }

    public void setIdPreconvention(int idPreconventon) {
        this.idPreconvention = idPreconventon;
    }

    public String getNomAssurance() {
        return nomAssurance;
    }

    public void setNomAssurance(String nomAssurance) {
        this.nomAssurance = nomAssurance;
    }

    public String getNumContrat() {
        return numContrat;
    }

    public void setNumContrat(String numContrat) {
        this.numContrat = numContrat;
    }

    public String getDateD() {
        return dateD;
    }

    public void setDateD(String dateD) {
        this.dateD = dateD;
    }

    public String getDateF() {
        return dateF;
    }

    public void setDateF(String dateF) {
        this.dateF = dateF;
    }

    public float getGratification() {
        return gratification;
    }

    public void setGratification(float gratification) {
        this.gratification = gratification;
    }

    public String getResume() {
        return resume;
    }

    public void setResume(String resume) {
        this.resume = resume;
    }

    public EtatPreConvention getEtat() {
        return etat;
    }

    public void setEtat(EtatPreConvention etat) {
        this.etat = etat;
    }

    public String getMotif() {
        return motif;
    }

    public void setMotif(String motif) {
        this.motif = motif;
    }

    public String getEnseignant() {
        return enseignant;
    }

    public void setEnseignant(String enseignant) {
        this.enseignant = enseignant;
    }

    public String getNiveauD() {
        return niveauD;
    }

    public void setNiveauD(String niveauD) {
        this.niveauD = niveauD;
    }

    public String getIntituleD() {
        return intituleD;
    }

    public void setIntituleD(String intituleD) {
        this.intituleD = intituleD;
    }

    public String getNumE() {
        return numE;
    }

    public void setNumE(String numE) {
        this.numE = numE;
    }

    public String getNomE() {
        return nomE;
    }

    public void setNomE(String nomE) {
        this.nomE = nomE;
    }

    public String getPrenomE() {
        return prenomE;
    }

    public void setPrenomE(String prenomE) {
        this.prenomE = prenomE;
    }

    public String getNumEnt() {
        return numEnt;
    }

    public void setNumEnt(String numEnt) {
        this.numEnt = numEnt;
    }

    public String getNomEnt() {
        return nomEnt;
    }

    public void setNomEnt(String nomEnt) {
        this.nomEnt = nomEnt;
    }

    @Override
    public String toString() {
        return "Preconvention{" + "idPreconventon=" + idPreconvention + ", nomAssurance=" + nomAssurance + ", numContrat=" + numContrat + ", dateD=" + dateD + ", dateF=" + dateF + ", gratification=" + gratification + ", resume=" + resume + ", etat=" + etat + ", motif=" + motif + ", enseignant=" + enseignant + ", niveauD=" + niveauD + ", intituleD=" + intituleD + ", numE=" + numE + ", nomE=" + nomE + ", prenomE=" + prenomE + ", numEnt=" + numEnt + ", nomEnt=" + nomEnt + '}';
    }

    
    
    
}
