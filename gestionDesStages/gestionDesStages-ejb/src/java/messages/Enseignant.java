/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package messages;

import java.io.Serializable;

/**
 * Objet Enseignant
 * @author Montuy & Martel
 */
public class Enseignant implements Serializable {
    private String numEns;
            
    private String nomEns;
            
    private String prenomEns;

    public Enseignant(String pNumEns, String pNomEns, String pPrenomEns) {
        this.numEns = pNumEns;
        this.nomEns = pNomEns;
        this.prenomEns = pPrenomEns;
    }
    public Enseignant() {
    }

    public String getNumEns() {
        return numEns;
    }

    public void setNumEns(String numEns) {
        this.numEns = numEns;
    }

    public String getNomEns() {
        return nomEns;
    }

    public void setNomEns(String nomEns) {
        this.nomEns = nomEns;
    }

    public String getPrenomEns() {
        return prenomEns;
    }

    public void setPrenomEns(String prenomEns) {
        this.prenomEns = prenomEns;
    }

    @Override
    public String toString() {
        return "Enseignant{" + "numEns=" + numEns + ", nomEns=" + nomEns + ", prenomEns=" + prenomEns + '}';
    }
    
    
}
