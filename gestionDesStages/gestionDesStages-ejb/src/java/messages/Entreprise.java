/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package messages;

import java.io.Serializable;

/**
 * Objet Entreprise
 * @author Montuy & Martel
 */
public class Entreprise implements Serializable {
    
    private String numSiren;
            
    private String nomEnt;       

    public Entreprise(String pNumSiren, String pNomEnt) {
        this.numSiren = pNumSiren;
        this.nomEnt = pNomEnt;
    }

    public Entreprise() {
    }
    
    public String getNumSiren() {
        return numSiren;
    }

    public void setNumSiren(String numSiren) {
        this.numSiren = numSiren;
    }

    public String getNomEnt() {
        return nomEnt;
    }

    public void setNomEnt(String nomEnt) {
        this.nomEnt = nomEnt;
    }

    @Override
    public String toString() {
        return "Entreprise{" + "numSiren=" + numSiren + ", nomEnt=" + nomEnt + '}';
    }
    
}
