/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package messages;

import java.io.Serializable;

/**
 * Objet Etudiant
 * @author Montuy & Martel
 */
public class Etudiant implements Serializable {
    private int numE;        
            
    private String prenomE;
    
    private String nomE;

    public Etudiant(int numE, String prenomE, String nomE) {
        this.numE = numE;
        this.prenomE = prenomE;
        this.nomE = nomE;
    }

    public Etudiant() {
    }

    public int getNumE() {
        return numE;
    }

    public void setNumE(int numE) {
        this.numE = numE;
    }

    public String getPrenomE() {
        return prenomE;
    }

    public void setPrenomE(String prenomE) {
        this.prenomE = prenomE;
    }

    public String getNomE() {
        return nomE;
    }

    public void setNomE(String nomE) {
        this.nomE = nomE;
    }

    @Override
    public String toString() {
        return "Etudiant{" + "numE=" + numE + ", prenomE=" + prenomE + ", nomE=" + nomE + '}';
    }
    
    
}
