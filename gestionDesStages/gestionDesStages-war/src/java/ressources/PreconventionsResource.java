/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ressources;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import messages.Preconvention;
import webserver.PreconventionsSingleton;

/**
 * REST Web Service
 * @author Montuy & Martel
 */
@Path("preconvention")
public class PreconventionsResource {

    /**
     * Initialisation de la préconventionSingleton
     */
    PreconventionsSingleton preconventionsSingleton = lookupCommandesSingletonBean();

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of PreconventionsResource
     */
    public PreconventionsResource() {
    }

    /**
     * POST methode : permet de créer une préconvention
     * @param content representation for the resource
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Preconvention postJson(Preconvention prev) {  
        System.out.println("ressources.PreconventionsResource.postJson()");
        System.out.println("ressources.PreconventionsResource.postJson()" + prev.toString());
        return preconventionsSingleton.creerPreconvention(
                prev.getNomAssurance(),
                prev.getNumContrat(),
                prev.getDateD(),
                prev.getDateF(),
                prev.getGratification(),
                prev.getResume(),            
                prev.getNiveauD(),
                prev.getIntituleD(),    
                prev.getNumE(),     
                prev.getNomE(),        
                prev.getPrenomE(),        
                prev.getNumEnt(),        
                prev.getNomEnt());
    }
    
    /**
     * Get méthode : Renvoie une instance d'une ressources.PreconventionsResource
     * @return an instance of Preconvention
     */
    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Preconvention getJson(@PathParam("id") int id) {
        return preconventionsSingleton.recupPreconvention(id);
    }
    
    /**
     * Get méthode : Récupération de toutes les préconventions validés.
     * @return an instance of Preconvention
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public ArrayList<Preconvention> getJson() {
        return preconventionsSingleton.recupListePreconventions();
    }
    
    /**
     * Get méthode : Récupération de la liste des entreprises où s'éffectue un stage.
     * @return an instance of java.lang.String
     */
    @GET
    @Path("Entreprises")
    @Produces(MediaType.APPLICATION_JSON)
    public ArrayList<String> getJsonSujets() {
        return preconventionsSingleton.recupListeEntreprises();
    }
    
    /**
     * Get méthode : Récupération de la liste des entreprises où s'éffectue un stage.
     * @return an instance of Preconvention
     */
    @GET
    @Path("NOK")
    @Produces(MediaType.APPLICATION_JSON)
    public ArrayList<Preconvention> getJsonPreconventionsNOK() {
        return preconventionsSingleton.recupListeStageNOK();
    }
    
    /**
     * Get méthode : Récupération de la liste des entreprises où s'éffectue un stage.
     * @return an instance of java.lang.int
     */
    @GET
    @Path("nbValide")
    @Produces(MediaType.APPLICATION_JSON)
    public int getNombreStageValidée() {
        return preconventionsSingleton.getNombreStageValidée();
    }

    /**
     * POST méthode : permet d'ajouter un enseignant-chercheur à une préconvention
     * @param content representation for the resource
     */
    @POST
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Preconvention ajouterEnseignantPreconvention(@PathParam("id") int id, int idEns) {
        return preconventionsSingleton.ajouterEnseignantPreconvention(id, idEns);
    }
    
    private PreconventionsSingleton lookupCommandesSingletonBean() {
        try {
            javax.naming.Context c = new InitialContext();
            return (PreconventionsSingleton) c.lookup("java:global/gestionDesStages/gestionDesStages-ejb/PreconventionsSingleton!webserver.PreconventionsSingleton");
        } catch (NamingException ne) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", ne);
            throw new RuntimeException(ne);
        }
    }


    ///////////////  Work in progress
   /* @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Preconvention postJson(@FormParam("NomAssurance") String NomAssurance,
                    @FormParam("numContrat") String numContrat,
                    @FormParam("dateD") String dateD,
                    @FormParam("dateF") String dateF,
                    @FormParam("gratification") int gratification,
                    @FormParam("resume") String resume,
                    @FormParam("niveauD") String niveauD,
                    @FormParam("intituleD") String intituleD,
                    @FormParam("numE") String numE,
                    @FormParam("nomE") String nomE,
                    @FormParam("prenomE") String prenomE,
                    @FormParam("numEnt") String numEnt,
                    @FormParam("nomEnt") String nomEnt) {  
        System.out.println("ressources.PreconventionsResource.postJson()");
        return preconventionsSingleton.creerPreconvention(
                NomAssurance,
                numContrat,
                dateD,
                dateF,
                gratification,
                resume,            
                niveauD,
                intituleD,    
                numE,     
                nomE,        
                prenomE,        
                numEnt,        
                nomEnt);
    }
    */
    
}
