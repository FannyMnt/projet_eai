package app.serviceJuridique;

import app.Nommage;
import com.google.gson.Gson;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Session;
import javax.jms.Topic;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import messages.EtatPreConvention;
import messages.Preconvention;
import messages.RepMessage;
import sirenRessources.Records;
import sirenRessources.SirenPOJO;

/**
 * Listener du service Juridique
 * @author Montuy & Martel
 */
public class JuridiqueListener implements MessageListener {

    /**
     * Producteur du service
     */
    private final MessageProducer mp;
    
    /**
     * Session JMS
     */
    private final Session session;

    /**
     * Constructeur de la classe
     * @param session session de la connection JMS
     * @param mp producteur du service
     */
    public JuridiqueListener(Session session, MessageProducer mp) {
        this.session = session;
        this.mp = mp;
        
    }

    @Override
    public void onMessage(Message message) {
        try {
            Topic source = (Topic) message.getJMSDestination();

            System.out.println("MSG RECU " + source.getTopicName());
            String topicName = source.getTopicName().replace('_', '/');
            String motif ="";

            if (source.getTopicName().equalsIgnoreCase(Nommage.TOPIC_PREV_EMISES)) {

                if (message instanceof ObjectMessage) {
                    ObjectMessage om = (ObjectMessage) message;
                    Object obj = om.getObject();
                    if (obj instanceof Preconvention) {
                        Preconvention prevRecu = (Preconvention) obj;
                        System.out.println("Preconvention");
                        System.err.println(prevRecu.toString());
                        System.out.println("Service Juridique : Préconvention " + prevRecu.getIdPreconvention() + " reçue ");
                        
                        boolean verifPeriode = true;
                        boolean verifAssur = true;
                        boolean verfiGeneral;
                        boolean verifGrat = true;
                        boolean verifDuree = true;
                        
                        //////////Vérification existence entreprise
                        System.out.println("prevRecu.getNumEnt() "+prevRecu.getNumEnt());
                        boolean verifEnt = appelSiren(prevRecu.getNumEnt());
                        if(verifEnt==false){
                            motif+="Le numéro de SIREN de l'entreprise n'est pas correct. \n";
                        }
                        System.out.println("verifEnt " + verifEnt);
                        
                        //////////Vérification responsabilité civile
                        if(prevRecu.getNumContrat().equals("testF")){
                            verifAssur = false;
                            motif+="Responsabilité civile incorect. \n";
                        }                 
                        System.out.println("verifAssur " + verifAssur);
                        // Ici, simu par tirage aléatoire
                        /*double r = Math.random();
                        if (r > 0.3) {
                            // Vérif OK (~ 70%)
                            verifAssur = true;
                            //cmd.setBanqueValide(true);
                            System.out.println("\t --> Ass. Responsabilité OK");
                        } else {
                            // Vérif KO (~ 30%)
                            verifAssur = false;
                            //cmd.setBanqueValide(false);
                            System.out.println("\t --> Ass. Responsabilité NOK");
                        }*/

                        //////////Vérification Général
                        
                            ////////////////////Vérification durée du stage
                            String dateD = prevRecu.getDateD();
                            String dateF = prevRecu.getDateF();
                            Calendar calStr1 = Calendar.getInstance();
                            Calendar calStr2 = Calendar.getInstance();
                            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yy");
                            try {
                                Date ddateD = sdf.parse(dateD);
                                Date ddateF = sdf.parse(dateF);
                                calStr1.setTime(ddateD);
                                calStr2.setTime(ddateF);

                                int nbMois = 0;
                                while (calStr1.before(calStr2)) {
                                    calStr1.add(GregorianCalendar.MONTH, 1);
                                    if (calStr1.before(calStr2) || calStr1.equals(calStr2)) {
                                            nbMois++;
                                    }
                                }
                                
                                if(nbMois>6) {
                                    verifDuree = false;
                                    motif+="Durée du stage incorect. La durée maximum est de 6 mois. \n";
                                }   
                                System.out.println("verifDuree " + verifDuree);

                                ////////////////////Vérification période
                                int mF = ddateF.getMonth()+1;
                                int yD = ddateD.getYear();
                                int yF = ddateF.getYear();
                                if(yD==yF){
                                    if(mF>=9) {
                                        verifPeriode = false;
                                        motif+="Période incorect. La période du stage ne doit pas être sur 2 années universitaire. \n";
                                    }
                                }
                                } catch (ParseException ex) {
                                    Logger.getLogger(JuridiqueListener.class.getName()).log(Level.SEVERE, null, ex);
                                }
                            System.out.println("verifPeriode " + verifPeriode);

                        ////////////////////Vérification montant gratification
                        
                        if(prevRecu.getGratification()<300) {
                            verifGrat = false;
                            motif+="Gratification incorect. La gratification est de 300 minimum. \n";
                        }
                           
                        System.out.println("verifGrat " + verifGrat);
                        

                        // envoi de la réponse de la banque
                        RepMessage mess;
                        if(verifAssur && verifEnt && verifGrat &&verifDuree && verifPeriode){
                            System.out.println("Le service juridique valide la preconvention "+ prevRecu.getIdPreconvention()+" !");
                            mess = new RepMessage(prevRecu.getIdPreconvention(), EtatPreConvention.VALIDEE, motif);
                        } else {
                            System.out.println("Le service juridique ne valide pas la preconvention "+ prevRecu.getIdPreconvention()+" !");
                            mess = new RepMessage(prevRecu.getIdPreconvention(), EtatPreConvention.ANNULEE, motif);
                        }
                            
                        ObjectMessage msg = session.createObjectMessage(mess);
                        msg.setJMSType(Nommage.MSG_JURIDIQUE);
                        mp.send(msg);
                    }
                }
            }
        } catch (JMSException ex) {
            Logger.getLogger(JuridiqueListener.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

       private boolean appelSiren(String siren) {
        // I/O JSON
        Gson gson = new Gson();

        // TOKEN BEARER a récuperer sur INSEE
        String token = "Bearer 87faafeb-b34f-39d4-8cc0-cb9e7a15a8d9";
        // URI Service INSEE
        String uri = "http://data.opendatasoft.com/api/records/1.0/search/?dataset=sirene%40public";

        // a ajuster selon requete voir mode emploi INSEE
        String query = "&lang=fr";

        Client client = ClientBuilder.newClient();
        WebTarget wt = client.target(uri + "&q=" + siren + query);

        //WebResource webResource = client.resource(uri + siren + query);
        System.out.println("uri appel: " + uri + "&q=" + siren + query);

        Invocation.Builder invocationBuilder = wt.request(MediaType.APPLICATION_JSON);
        Response response = invocationBuilder.get();
        String reponse = response.readEntity(String.class);

        // Convertisseur JSON
        SirenPOJO model = gson.fromJson(reponse, SirenPOJO.class);

        System.out.println("Résultat: " + response.getStatus());
        Records [] rec = model.getRecords();
        if (rec.length == 0)
            return false;
        
        return true;
    }
    

}
