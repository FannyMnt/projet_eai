package app;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.naming.NamingException;
import messages.EtatPreConvention;
import messages.RepMessage;

/**
 * Service de gestion des stages
 * @author Montuy & Martel
 */
public class GestionStage extends ClientJMS {

    /**
     * Consommateur du service
     */
    private MessageConsumer mc;
    
    /**
     * Producteur du service
     */
    private MessageProducer mprod;

    private final HashMap<Integer, ArrayList<RepMessage>> prevEnAttente;    

    /**
     * Constructeur par défaut
     */
    public GestionStage() {
        prevEnAttente = new HashMap();
    }

    /**
     * Initialisation des prodcuteurs et des consommateurs
     */
    private void setProducerConsumer() {

        try {

            //recuperation des destinations
            Destination prevEnCours = (Destination) namingContext.lookup(Nommage.QUEUE_PREV_ENCOURS);
            Destination prevGestPrev = (Destination) namingContext.lookup(Nommage.TOPIC_GEST_PREV);
            System.out.println("Destination lookup done.");

            // creation des consommateurs et du producteur
            mc = session.createConsumer(prevEnCours);
            mprod = session.createProducer(prevGestPrev);

        } catch (JMSException | NamingException ex) {
            Logger.getLogger(GestionStage.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Traitement des messages reçus
     * @param msg message récupéré d'un service
     */
    private void processMessage(Message msg) {

        try {
            RepMessage repM;
            if (msg instanceof ObjectMessage) {
                ObjectMessage om = (ObjectMessage) msg;
                Object obj = om.getObject();
                if (obj instanceof RepMessage) {
                    repM = (RepMessage) obj;
                } else {
                    return;
                }
            } else {
                return;
            }
            System.out.println("app.GestionStage.processMessage()  repM "+repM.getEtat());
            if (prevEnAttente.containsKey(repM.getIdPreconventon()) ) {
                // premier message deja recue
                if( prevEnAttente.get(repM.getIdPreconventon()).size()<2){
                    System.out.println("on enregistre le 2 ème message");
                    prevEnAttente.get(repM.getIdPreconventon()).add(repM);
                    System.out.println(prevEnAttente.get(repM.getIdPreconventon()).toString());
                } else {
                    //3 ème meesage reçu
                    RepMessage mess;
                    String motif="";
                    String enseignant="";
                    prevEnAttente.get(repM.getIdPreconventon()).add(repM);
                    ArrayList<RepMessage> listMessages = prevEnAttente.get(repM.getIdPreconventon());
                    boolean etat = true;
                    for (RepMessage rM : listMessages ){
                        System.out.println("app.GestionStage.processMessage()  "+ rM.getEtat());
                        if("ANNULEE"==rM.getEtat().toString()){
                            etat = false;
                            motif+=rM.getMotif();
                        }
                        if (rM.getEnseignant()!="") {
                            enseignant = rM.getEnseignant();
                        }
                    }
                   
                    if (etat==true) {
                     // tout est OK
                        mess = new RepMessage(repM.getIdPreconventon(), EtatPreConvention.VALIDEE, motif,enseignant); 
                        ObjectMessage om = session.createObjectMessage(mess);
                        mprod.send(om);
                        System.out.println("\t Preconvention " + repM.getIdPreconventon() + " valide.");
                    } else {
                        // non valide, on annule
                        mess = new RepMessage(repM.getIdPreconventon(), EtatPreConvention.ANNULEE, motif); 
                        ObjectMessage om = session.createObjectMessage(mess);
                        mprod.send(om);
                        System.out.println("\t Preconvention " + repM.getIdPreconventon() + " non valide, on l'annule.");
                    }
                }
                
                
            } else {
                // nouvelle préconvention, on l'ajoute dans le dict
                ArrayList<RepMessage> liste = new ArrayList<>();
                liste.add(repM);
                System.out.println("repM.getIdPreconventon()" + repM.getIdPreconventon());
                prevEnAttente.put(repM.getIdPreconventon(), liste);
                System.out.println("--> RepMessage " + repM.getIdPreconventon() + " mise en attente.");
            }
        } catch (Exception ex) {
            Logger.getLogger(GestionStage.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * @param args the command line arguments
     * @throws java.lang.Exception
     */
    public static void main(String[] args) throws Exception {

        boolean theEnd = false;

        GestionStage gc = new GestionStage();
        gc.initJMS();
        gc.setProducerConsumer();
        gc.startJMS();
        System.out.println("*** Service de Gestion de Stages démarré. ***");

        do {
            Message msg = gc.mc.receive();

            System.out.println("----------");
            gc.processMessage(msg);
        } while (!theEnd);
        gc.closeJMS();
    }

}
