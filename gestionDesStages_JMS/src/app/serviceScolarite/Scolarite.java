package app.serviceScolarite;

import app.ClientJMS;
import app.Nommage;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.logging.Logger;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.naming.NamingException;

/**
 * Service Scolarite
 * @author Montuy & Martel
 */
public class Scolarite extends ClientJMS {
    
    /**
     * Consommateur du service
     */
    private MessageConsumer mc1;
    
    /**
     * Producteur du service
     */
    private MessageProducer mprod;

    /**
     * Constructeur par défaut
     */
    public Scolarite() {
    }

    /**
     * Initialisation des prodcuteurs et des consommateurs
     */
    private void setProducerConsumer() {
        try {
            // recuperation des destinations
            Destination prevEmises = (Destination) namingContext.lookup(Nommage.TOPIC_PREV_EMISES);
            Destination prevEnCours = (Destination) namingContext.lookup(Nommage.QUEUE_PREV_ENCOURS);
            System.out.println("Destination lookup done.");

            // creation des consommateurs et du producteur
            mc1 = session.createConsumer(prevEmises);
            mprod = session.createProducer(prevEnCours);

            // Ajout du Listener au producteur.
            ScolariteListener scolisten = new ScolariteListener(session, mprod);
            mc1.setMessageListener(scolisten);

        } catch (JMSException | NamingException ex) {
            Logger.getLogger(ex.toString());
        }
    }

    /**
     * @param args the command line arguments
     * @throws java.lang.Exception
     */
    public static void main(String[] args) throws Exception {

        Scolarite monServiceScolarite = new Scolarite();
        monServiceScolarite.initJMS();
        monServiceScolarite.setProducerConsumer();
        monServiceScolarite.startJMS();
        System.out.println("*** Service de scolarite démarré. ***");
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        do {
            System.out.println("Appuyez sur 'Q' pour quitter.");
        } while (!br.readLine().equalsIgnoreCase("Q"));
        monServiceScolarite.closeJMS();
    }

}
