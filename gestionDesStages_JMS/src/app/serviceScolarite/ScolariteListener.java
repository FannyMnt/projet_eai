package app.serviceScolarite;

import app.Nommage;
import app.serviceJuridique.JuridiqueListener;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Session;
import javax.jms.Topic;
import messages.EtatPreConvention;
import messages.Preconvention;
import messages.RepMessage;

/**
 * Listener du service Scolarité
 * @author Montuy & Martel
 */
public class ScolariteListener implements MessageListener {

    /**
     * Producteur du service
     */
    private final MessageProducer mp;
    
    /**
     * Session JMS
     */
    private final Session session;

    /**
     * Constructeur de la classe
     * @param session session de la connection JMS
     * @param mp producteur du service
     */
    public ScolariteListener(Session session, MessageProducer mp) {
        this.session = session;
        this.mp = mp;
    }

    @Override
    public void onMessage(Message message) {

        try {
            Topic source = (Topic) message.getJMSDestination();
            String motif="";
            // System.out.println("MSG RECU " + source.getTopicName());
            String topicName = source.getTopicName().replace('_', '/');

            if (source.getTopicName().equalsIgnoreCase(Nommage.TOPIC_PREV_EMISES)) {

                if (message instanceof ObjectMessage) {
                    ObjectMessage om = (ObjectMessage) message;
                    Object obj = om.getObject();
                    if (obj instanceof Preconvention) {
                        Preconvention prevRecu = (Preconvention) obj;
                        System.out.println("Service Scolarite : Préconvention " + prevRecu.getIdPreconvention() + " reçue ");
                        
                        RepMessage mess;
                        
                        // Ici, simu par tirage aléatoire
                        double r = Math.random();
                        if (r > 0.3) {
                            // Vérif OK (~ 70%)
                            mess = new RepMessage(prevRecu.getIdPreconvention(), EtatPreConvention.VALIDEE, motif);
                            System.out.println("Le service scolarité valide la preconvention "+ prevRecu.getIdPreconvention()+" !");
                        } else {
                            // Vérif NOK (~ 30%)
                            motif+="Problème au niveau de la scolarité ! \n";
                            mess = new RepMessage(prevRecu.getIdPreconvention(), EtatPreConvention.ANNULEE, motif);
                            System.out.println("Le service scolarité ne valide pas la preconvention "+ prevRecu.getIdPreconvention()+" !");
                        }
                        
                        // envoi de la réponse 
                        ObjectMessage msg = session.createObjectMessage(mess);
                        msg.setJMSType(Nommage.MSG_SCOLARITE);
                        mp.send(msg);
                    }
                }
            }

        } catch (JMSException ex) {
            Logger.getLogger(JuridiqueListener.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private static boolean verifierCoordonnees(java.lang.String iban) {
    /*    clients_ws.ServiceBanque_Service service = new clients_ws.ServiceBanque_Service();
        clients_ws.ServiceBanque port = service.getServiceBanquePort();
        return port.verifierCoordonnees(iban);
*/return true;
    }

}
