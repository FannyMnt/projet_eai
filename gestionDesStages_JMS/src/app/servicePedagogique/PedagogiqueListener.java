package app.servicePedagogique;

import app.Nommage;
import app.serviceJuridique.JuridiqueListener;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Session;
import javax.jms.Topic;
import messages.EtatPreConvention;
import messages.Preconvention;
import messages.RepMessage;

/**
 * Listener du service Pédagogique
 * @author Montuy & Martel
 */
public class PedagogiqueListener implements MessageListener {

    /**
     * Consommateur de la classe
     */
    private final MessageProducer mp;
    
    /**
     * Producteur de la classe
     */
    private final Session session;
    
    /**
     * Autres attributs
     */
    private static Scanner sc;
    private RepMessage mess;
    private ObjectMessage msg;

    /**
     * Constructeur de la classe
     * @param session session JMS
     * @param mp producteur du service
     */
    public PedagogiqueListener(Session session, MessageProducer mp) {
        this.session = session;
        this.mp = mp;
    }

    @Override
    public void onMessage(Message message) {

        try {
            Topic source = (Topic) message.getJMSDestination();
            sc = new Scanner(System.in);
            String motif="";

            if (source.getTopicName().equalsIgnoreCase(Nommage.TOPIC_PREV_EMISES)) {

                if (message instanceof ObjectMessage) {
                    System.out.println(message.toString());
                    ObjectMessage om = (ObjectMessage) message;
                    System.out.println("app.servicePedagogique.PedagogiqueListener.onMessage()");
                    System.out.println(om.getObject().toString());
                    Object obj = om.getObject();
                    if (obj instanceof Preconvention) {
                        Preconvention prevRecu = (Preconvention) obj;
                        System.out.println("Service Pédagogique : Préconvention " + prevRecu.getIdPreconvention() + " reçue ");
                        
                        String choix="";
                        //////Vérification cohérence du stage
                        System.out.println("Veuillez vérifier la cohérence du sujet du stage de l'éléève suivant :");
                        System.out.println("    Etudiant : " + prevRecu.getNomE() + "  " + prevRecu.getPrenomE());
                        System.out.println("    Diplome préparé : " + prevRecu.getNiveauD() + "  " + prevRecu.getIntituleD());
                        System.out.println("    Résumé du stage : "+ prevRecu.getResume());
                        
                      
                        System.out.println("Saisissez y pour oui et n pour non : ");

                        choix = sc.next();
                        System.out.println("choix " + choix);
                        //Gestion des choix
                        switch(choix) { 
                            case "n" :
                                // envoi de la réponse à la gestion des stages
                                motif+="Le sujet du stage n'est pas cohérent avec la formation ! \n";
                                mess = new RepMessage(prevRecu.getIdPreconvention(), EtatPreConvention.ANNULEE, motif);
                                System.out.println("Le service pédagogique ne valide pas la preconvention "+ prevRecu.getIdPreconvention()+" !");
                                break;
                            case "y": 
                                System.out.println("\t --> Sujet du stage OK");
                            ////Vérification de la durée envisagée du stapge par rapport à l'UE                                    
                            // Ici, simu par tirage aléatoire
                                double r = Math.random();
                                if (r <= 0.3) {
                                    // envoi de la réponse à la gestion des stages
                                    motif+="La durée du stage n'est pas cohérente avec l'UE ! \n";
                                    mess = new RepMessage(prevRecu.getIdPreconvention(), EtatPreConvention.ANNULEE, motif);
                                    System.out.println("\t --> Durée du stage par rapport à l'UE NOK");
                                    System.out.println("Le service pédagogique ne valide pas la preconvention "+ prevRecu.getIdPreconvention()+" !");

                                } else {
                                    motif+="La durée du stage est cohérente avec l'UE ! \n";
                                    System.out.println("Veuillez affecter l'enseignant chercheur :");
                                    String enseignant = sc.next() + " " + sc.next();
                                    prevRecu.setEnseignant(enseignant);
                                    System.out.println("Enseignant affecté");
                                    mess = new RepMessage(prevRecu.getIdPreconvention(), EtatPreConvention.VALIDEE, motif, enseignant);
                                    System.out.println("Le service pédagogique valide la preconvention "+ prevRecu.getIdPreconvention()+" !");
                                }
                                break;

                            default: System.out.println("Vous n'avez selectionné‚ aucun choix ");  
                        }
                        
                        //Envoie de la réponse du service
                        msg = session.createObjectMessage(mess);
                        msg.setJMSType(Nommage.MSG_PEDAGOGIQUE);
                        mp.send(msg);
 
                    }
                }
            }

        } catch (JMSException ex) {
            Logger.getLogger(JuridiqueListener.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
