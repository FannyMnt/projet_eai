package app.servicePedagogique;

import app.ClientJMS;
import app.Nommage;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.logging.Logger;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.naming.NamingException;

/**
 * Service Pédagogique
 * @author Montuy & Martel
 */
public class Pedagogique extends ClientJMS {

    /**
     * Consommateur du service
     */
    private MessageConsumer mc1;
    
    /**
     * Producteur du service
     */
    private MessageProducer mprod;

    /**
     * Constructeur par défaut
     */
    public Pedagogique() {
    }

    /**
     * Initialisation des prodcuteurs et des consommateurs
     */
    private void setProducerConsumer() {
        try {
          
            // recuperation des destinations
            Destination prevEmises = (Destination) namingContext.lookup(Nommage.TOPIC_PREV_EMISES);
            Destination prevEnCours = (Destination) namingContext.lookup(Nommage.QUEUE_PREV_ENCOURS);
            System.out.println("Destination lookup done.");

            // creation des consommateurs et du producteur
            mc1 = session.createConsumer(prevEmises);
            mprod = session.createProducer(prevEnCours);

            // Ajout du Listener au producteur.
            PedagogiqueListener pedlisten = new PedagogiqueListener(session, mprod);
            mc1.setMessageListener(pedlisten);

        } catch (JMSException | NamingException ex) {
            Logger.getLogger(ex.toString());
        }

    }

    /**
     * @param args the command line arguments
     * @throws java.lang.Exception
     */
    public static void main(String[] args) throws Exception {

        Pedagogique monServicePedagogique = new Pedagogique();
        monServicePedagogique.initJMS();
        monServicePedagogique.setProducerConsumer();
        monServicePedagogique.startJMS();
        System.out.println("*** Service pedagogique démarré. ***");
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Appuyez sur 'Q' pour quitter.");
        int i =0;
        do {
        } while (i==0);
        
        monServicePedagogique.closeJMS();
    }

}
