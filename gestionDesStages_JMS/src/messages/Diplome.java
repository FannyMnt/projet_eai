/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package messages;

import java.io.Serializable;

/**
 * Objet Diplome
 * @author Montuy & Martel
 */
public class Diplome implements Serializable {
    
    private int numD;
    
    private String niveau;
    
    private String intitule;
    
    public Diplome(int pnumD, String pNiveau, String pIntitule) {
        this.numD = pnumD;
        this.niveau = pNiveau;
        this.intitule = pIntitule;
    }

    public Diplome() {
    }

    public int getNumD() {
        return numD;
    }

    public void setNumD(int numD) {
        this.numD = numD;
    }

    public String getNiveau() {
        return niveau;
    }

    public void setNiveau(String niveau) {
        this.niveau = niveau;
    }

    public String getIntitule() {
        return intitule;
    }

    public void setIntitule(String intitule) {
        this.intitule = intitule;
    }

    @Override
    public String toString() {
        return "Diplome{" + "numD=" + numD + ", niveau=" + niveau + ", intitule=" + intitule + '}';
    }
    
    
}
